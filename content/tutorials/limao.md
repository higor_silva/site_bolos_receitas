---
title: "Bolo de Limão"
date: 2021-09-18T23:28:40-03:00
draft: false
featured_image: images/bolo_de_limao.jpg
---

# Massa: 
2 ovos (claras em neve)

1 e 1/2 xícara de açucar

3 colheres (sopa) margarina

suco de 1 ou 2 limões (depende do tamano)
raspas casca limão

3 e ¹/2 xícaras de farinha de trigo

1 xícara de leite

2 colheres (sopa) fermento em pó

# Cobertura:
2/3 lata de leite condensado
raspas da casca de 1 limão pequeno
suco de 1/2 limão